module GitBreeze
  module Standalone
    extend self

    GIT_BREEZE_ROOT = File.expand_path('../../..', __FILE__)
    PREAMBLE = <<-preamble
#
# This file is generated code. DO NOT send patches for it.
#
# Original source files with comments are at:
# https://bitbucket.org/mobileigniter/git_breeze
#

preamble

    def save(filename, path = '.')
      dest = File.join(File.expand_path(path), filename)
      File.open(dest, 'w') do |f|
        build(f)
        f.chmod(0755)
      end
    end

    def build(io)
      io.puts "#!#{ruby_executable}"
      io << PREAMBLE

      each_source_file do |filename|
        File.open(filename, 'r') do |source|
          inline_source(source, io)
        end
      end

      io.puts 'GitBreeze::Runner.execute(*ARGV)'
      io
    end

    def ruby_executable
      if File.executable? '/usr/bin/ruby' then '/usr/bin/ruby'
      else
        require 'rbconfig'
        File.join(RbConfig::CONFIG['bindir'], RbConfig::CONFIG['ruby_install_name'])
      end
    end

    private

    def inline_source(code, io)
      code.each_line do |line|
        io << line unless require_own_file?(line)
      end
      io.puts ''
    end

    def require_own_file?(line)
      line =~ /^\s*require\s+["']git_breeze\//
    end

    def each_source_file
      File.open(File.join(GIT_BREEZE_ROOT, 'lib/git_breeze.rb'), 'r') do |main|
        main.each_line do |req|
          if req =~ /^require\s+["'](.+)["']/
            yield File.join(GIT_BREEZE_ROOT, 'lib', "#{$1}.rb")
          end
        end
      end
    end

  end
end
