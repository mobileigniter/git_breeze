require 'git_breeze/prepare_commit_message'
require 'git_breeze/hook'
require 'git_breeze/version'

module GitBreeze
  module Runner

    def self.execute(cmd_arg = 'help', *args)
      command = cmd_arg.gsub(/-/, '_')
      abort("[git_breeze] command: '#{cmd_arg}' does not exist.") unless respond_to?(command)
      send(command, *args)
    end

    def self.prepare_commit_msg(*args)
      PrepareCommitMessage.run(*args)
    end

    def self.init
      Hook.init
    end

    def self.install
      puts '`git-breeze install` is deprecated. Please use `git-breeze init`'
      self.init
    end

    def self.help
      puts <<-HELP
git-breeze #{VERSION} is installed.

Remember, git-breeze is a hook which Git interacts with during its normal
lifecycle of committing, rebasing, merging, etc. You need to initialize this
hook by running `git-breeze init` from each repository in which you wish to
use it. Cheers!
      HELP
    end
  end

end
