# -*- encoding: utf-8 -*-
require File.expand_path('../lib/git_breeze/version', __FILE__)

Gem::Specification.new do |gem|
    gem.name          = 'git_breeze'
    gem.version       = GitBreeze::VERSION
  gem.authors       = ['Timothy Nott']
    gem.email         = ['tim@mobileigniter.com']
  gem.homepage      = 'https://bitbucket.org/mobileigniter/git_breeze'
    gem.summary       = %q{Teaching Git about Breeze.}
  gem.description   = <<-EOF
Some simple tricks that make working with Breeze.pm easier
  EOF

  gem.add_development_dependency 'rspec', '~> 3.2.0'
  gem.add_development_dependency 'activesupport', '~> 3.2'
  # Use i18n < 0.7 until we drop Ruby 1.8.7 and 1.9.2 support
  gem.add_development_dependency 'i18n', '~> 0.6.11'
  gem.add_development_dependency 'pry', '~> 0.10.1'
  # Use Rake < 10.2 (requires Ruby 1.9+) until we drop Ruby 1.8.7 support
  gem.add_development_dependency 'rake', '~> 10.1.1'

  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
gem.executables   = %w(git-breeze)
  gem.require_paths = ['lib']
end
