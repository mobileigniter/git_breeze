### 1.1.7 / 2015-09-10
Fixed hook command

### 1.1.7 / 2015-09-10
added ability to move a 'move command' from the text of a commit to its end

### 1.0.12 / 2015-09-09
A bunch of fixes to the message portion

### 0.1.0 / 2015-09-09
changed references from tracker to brreeze and altered the code to remove unnecessary stuff

### 0.0.1 / 2015-09-09
[full changelog](https://github.com/stevenharman/git_tracker/compare/v0.0.1...v1.0.0)

Forked from git_tracker
